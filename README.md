# ANDES RIZ PSF size / Dispersion measurement

## Old version from the DDE Api from Zemax

To run this one needs :
* jupyter notebook
* The module pyzdde


In the zemax files one needs :

* to use the multiconfiguration editor with the following  operands:

|#   |OPERAND | comments |
|--- |---     | ---      |
|1   |WAVE 1  |          |
|2   |WAVE 2  |          |
|3   |WAVE 3  |          |
|4   |PRAM N/2| N is the surface number of the echelle |


* to have the merit function as folow: 

|#  |OPERAND| comments |
|--- |---    | ---      |
|1| CONF  | 1 |
|2| XENC  | Type 2, Frac 0.8|
|3| XENC  | Type 3, Frac 0.8|

* To produce config files for the extended encircled energy analysis, where one uses field 1, wave 1, chose the fiber shape (circular for example) and define the size of the fiber. If one wants different sizes for the fiber one need one file per size. in order not to lose these file copy them under a different name from the one of the zemax file. This latter file is overwritten at each analysis.

## New implmentation using the zos-API and the zospy package

Requirements:
* jupyter notebook
* tqdm
* zospy package

The routines for analysis are in the spectro_analysis_zospy.py
The notebook in ANDES_analysis_zospy.ipynb

The ExtendedImagesSimulation.py add some analyses to the Zospy package, as its names states it is for extendedimage simulations

The ThermalSensityZospy.ipynb have the thermal analysis updated for zospy, and some intial simulation comparing how simultaneous calibration could work.
