import pyzdde.zdde as pyz
from tqdm.notebook import tqdm
from pylab import *
from astropy.modeling import models, fitting
import os
import pathlib
import time
import shutil

from contextlib import contextmanager

@contextmanager
def pyzemax(*args, **kwds):
    """
    Class to simplify the link with ZEMAX and automate the closing of the link once finished
    """
    # Code to acquire resource, e.g.:
    ln = pyz.createLink()
    try:
        yield ln
    finally:
        # Code to release resource, e.g.:
        pyz.closeLink()



# some useful variables for the spiral plots
hx = 0.0
hy = 0.0
spirals = 5 #100
rays = 200   #6000




    
class spectro:
    """
    Class to store the spectrograph main caracteristics
    Bldeg: blaze angle in degrees
    opt_d: grating period in lines/mm
    omin: order minimum
    omax: order maximum
    """
    def __init__(self, Bldeg, opt_d, omin, omax):
        self.Bldeg = Bldeg
        self.opt_d = opt_d
        self.omin = omin
        self.omax = omax
    def __str__(self):
        return str(vars(self))
    
def wave_order(spectro, order=88, nwaves=10):
    """
    Gives an verctor sampling an order FSR for a given spectro
    inputs: 
        spectro: class that describes the main characteristics of the spectro
        order: order that needs to be sambpled
        nwaves: number of wavelength to output
    outputs:
        waves: vector of wavelenght covering the order
    
    """
    c_m = 0.5
    lcen = 2*sin(spectro.Bldeg*pi/180)/(order*spectro.opt_d)*1000
    limin = lcen - c_m *lcen/ order
    limax = lcen + c_m * lcen / order
    return linspace(limin,limax,nwaves)


def enslited_energy_spectro(spectro, nwaves = 10, hx=0, hy=0, field = 0, 
                            cname = "/home/chazelas/Documents/travail/ANDES/Phase_B/Design/optics/psfsize_dispersion/HIRES_v29_R_643.CFG"):
    """
    Analyze the enlited energy of the PSF of the spectro
    inputs:
        spectro: class that describes the main charateristics of the spectro
        nwaves: number of waves per order where the PSF will be computed
        hx,hy: coordinate of the field at the input in zemax coordinates for computing the position of the wavelength on the detector
        field: Position of the fiber in the entrance slit in mm
        cname: name of the config file that has the right fiber size definition. It will be used to overwrite the current configuration file for the enslited energy analysis
    outputs:
        data: a table with norders * nwave lines. The column are: 
            order, wavelength, xwave in mm, ywave in mm,
            half 80% enslited energy size in x in microns, 
            half 80% enslited energy size in y in microns  
    """
    with pyzemax() as ln:
        fname = ln.zGetFile()
        p = pathlib.Path(".")
        p = p / fname.replace(".zmx",".CFG").replace(".ZMX",".CFG")
        try:
            shutil.copy(cname,p)
        except shutil.SameFileError:
            pass
        ln.zSetConfig(1) # use config 1
        f = ln.zGetField(1)
        la  = ln.zGetWave(1)
        ln.zSetField(1,0,field,1)
        orderini= ln.zGetMulticon(1,4)
        orders = range(spectro.omin,spectro.omax+1)
        data = []
        for o in tqdm(orders):
            waves = wave_order(spectro,o, nwaves)
            for l in range(nwaves):
                ln.zSetMulticon(1, 1, waves[l], 0, 0, 0, 0, 0) # set the wave using the wave 1 in the MCE
                ln.zSetMulticon(1, 4, o, 0, 0, 0, 0, 0) # set the order on the echelle operand 4 in the MCE
                ln.zPushLens()
                (xb,yb,zb,intensityb) = ln.zSpiralSpot(hx,hy,1,3,10) # position of the wave on the detector
                ln.zOptimize(-1)
                x80=float(ln.zGetOperand(2,10)) # x 80% energy enslitted energy operand xenc has to exist in the merit function editor
                y80=float(ln.zGetOperand(3,10)) # y 80% energy enslitted energy operand xenc has to exist in the merit function editor
                l = (o,waves[l],mean(xb),mean(yb),x80,y80)
                data.append(l)
        ln.zSetMulticon(1, 1, la.wavelength, 0, 0, 0, 0, 0)
        ln.zSetMulticon(1, 4, orderini.value, 0, 0, 0, 0, 0)
        ln.zSetField(1,f.xf,f.yf,1)
        ln.zPushLens()
        
        data = array(data)
        figure(1)
        scatter(data[:,2],data[:,3],data[:,4],c=data[:,4]/10*2)
        colorbar(label = "X 80% energy in pixels")
        figure(2)
        scatter(data[:,2],data[:,3],data[:,5],c=data[:,5]/10*2)
        colorbar(label = "Y 80% energy in pixels")
        return data
        
        
def geometrical_dispersion_spectro(spectro,nwaves = 10,hx=0,hy=0,field = 0, R = 100000):
    """
    Analyze the enlited energy of the PSF of the spectro
    inputs:
        spectro: class that describes the main charateristics of the spectro
        nwaves: number of waves per order where the PSF will be computed
        hx,hy: coordinate of the field at the input in zemax coordinates for computing the position of the wavelength on the detector
        field: Position of the fiber in the entrance slit in mm
        R = resolution
    outputs:
        data: a table with norders * nwave lines. The column are: 
            order, wavelength, xwave in mm, ywave in mm,
            distance between 2 resolution element in micron along x axis,
            distance between 2 resolution element in micron 
    """
    with pyzemax() as ln:
        ln.zSetConfig(1)
        f = ln.zGetField(1)
        la  = ln.zGetWave(1)
        ln.zSetField(1,0,field,1)
        orderini= ln.zGetMulticon(1,4)
        orders = range(spectro.omin,spectro.omax+1)
        data = []
        for o in tqdm(orders):
            waves = wave_order(spectro,o, nwaves)
            for l in range(nwaves):
                ln.zSetMulticon(1, 1, waves[l], 0, 0, 0, 0, 0)
                ln.zSetMulticon(1, 4, o, 0, 0, 0, 0, 0)
                ln.zPushLens()
                (xb,yb,zb,intensityb) = ln.zSpiralSpot(hx,hy,1,3,10) # position of current wave l
                ln.zSetMulticon(1, 1, waves[l]+waves[l]/R, 0, 0, 0, 0, 0)
                ln.zPushLens()
                (xd,yd,zd,intensityd) = ln.zSpiralSpot(hx,hy,1,3,10) # position of the wave l+l/R
                l = (o,waves[l],mean(xb),mean(yb),abs(mean(xd)-mean(xb))*1000,sqrt((mean(xd)-mean(xb))**2+(mean(yd)-mean(yb))**2)*1000)
                data.append(l)
        ln.zSetMulticon(1, 1, la.wavelength, 0, 0, 0, 0, 0)
        ln.zSetMulticon(1, 4, orderini.value, 0, 0, 0, 0, 0)
        ln.zSetField(1,f.xf,f.yf,1)
        ln.zPushLens()
        
        data = array(data)
        figure(1)
        scatter(data[:,2],data[:,3],data[:,4],c=data[:,4]/10)
        colorbar(label = f"pixel for $\Delta\lambda / \lambda$ = {R}")
        figure(2)
        scatter(data[:,2],data[:,3],data[:,5],c=data[:,5]/10)
        colorbar(label = f"pixel for $\Delta\lambda / \lambda$ = {R}")
        return data