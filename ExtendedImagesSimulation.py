import zospy as zp
import pandas as pd

from zospy.analyses.base import AnalysisResult, OnComplete, new_analysis
from zospy import utils

def GeometricalImageAnalysis(
    oss,
    field_size : float = 0,
    image_size : float = 0,
    wavelength: str | int = 1,
    field :str | int = 1,
    file :str = "CIRCLE.IMA",
    rotation :float = 0,
    rays :float = 0,
    surface :str | int = "Image",
    n_pixels :int = 100,
    na : float = 0,
    reference: str = "Chief Ray",
    remove_vignetting_factors: bool = True,
    delete_vignetted: bool = True,
    use_polar: bool = False,
    oncomplete: OnComplete | str = OnComplete.Close,
) -> AnalysisResult:
    analysis_type = zp.constants.Analysis.AnalysisIDM.GeometricImageAnalysis

    analysis = new_analysis(oss, analysis_type)
    analysis.set_wavelength(wavelength)
    analysis.set_field(field)
    analysis.set_surface = surface
    analysis.Settings.ImageSize = image_size
    analysis.Settings.FieldSize = field_size
    analysis.Settings.RaysX1000 = rays
    analysis.Settings.Rotation = rotation
    analysis.Settings.NA = na
    analysis.Settings.NumberOfPixels = n_pixels
    analysis.Settings.File = file
    analysis.Settings.Reference = zp.constants.process_constant(zp.constants.Analysis.Settings.ReferenceGia,reference)
    analysis.Settings.RemoveVignettingFactors = remove_vignetting_factors
    analysis.Settings.UsePolarization = use_polar
    analysis.Settings.DeleteVignetted = delete_vignetted
    
    analysis.ApplyAndWaitForCompletion()
    # Get headerdata, metadata and messages
    headerdata = analysis.get_header_data()
    #data = analysis.get_data()
    metadata = analysis.get_metadata()
    messages = analysis.get_messages()


    

    # Get data
    if analysis.Results.NumberOfDataGrids <= 0:
        data = None
    elif analysis.Results.NumberOfDataGrids == 1:
        data = zp.utils.zputils.unpack_datagrid(analysis.Results.DataGrids[0])
    else:
        data = AttrDict()
        for ii in range(analysis.Results.NumberOfDataGrids):
            desc = analysis.Results.DataGrids[ii].Description
            key = desc if desc != "" else str(ii)
            data[key] = zp.utils.zputils.unpack_datagrid(analysis.Results.DataGrids[ii])

    
    # Create output
    result = AnalysisResult(
        analysistype=str(analysis_type),
        data=data,
        settings=None,
        metadata=metadata,
        headerdata=headerdata,
        messages=messages,
    )

    return analysis.complete(oncomplete, result)


def ExtendedSourceEncircledEnergy(
    oss,
    field_size : float = 0,
    image_size : float = 0,
    wavelength: str | int = 1,
    field :str | int = 1,
    file :str = "CIRCLE.IMA",
    rays :float = 0,
    surface :str | int = "Image",
    maximum_distance: float = 200,
    type: str = "X Only",
    refer_to : str = "Chief Ray",
    use_polar: bool = False,
    remove_vignetting_factors: bool= True,
    oncomplete: OnComplete | str = OnComplete.Close,
) -> AnalysisResult:
    analysis_type = zp.constants.Analysis.AnalysisIDM.ExtendedSourceEncircledEnergy

    analysis = new_analysis(oss, analysis_type)
    #print(analysis)
    #print(wavelength,isinstance(wavelength,int))
    analysis.set_wavelength(wavelength)
    analysis.set_field(field)
    analysis.set_surface = surface
    analysis.Settings.ImageSize = image_size
    analysis.Settings.FieldSize = field_size
    analysis.Settings.RaysX1000 = rays
    analysis.Settings.File = file
    analysis.Settings.MaximumDistance = maximum_distance
    analysis.Settings.ReferTo = zp.constants.process_constant(zp.constants.Analysis.Settings.ReferenceGia,refer_to)
    analysis.Settings.UsePolarization = use_polar
    analysis.Settings.RemoveVignettingFactors = remove_vignetting_factors
    

    analysis.ApplyAndWaitForCompletion()
    # Get headerdata, metadata and messages
    headerdata = analysis.get_header_data()
    #data = analysis.get_data()
    metadata = analysis.get_metadata()
    messages = analysis.get_messages()


 

    data = []
    for ii in range(analysis.Results.NumberOfDataSeries):
        data.append(utils.zputils.unpack_dataseries(analysis.Results.DataSeries[ii]))

    if not len(data):
        data = pd.DataFrame()
    elif len(data) == 1:
        data = data[0]
    else:
        data = pd.concat(data, axis=1)


    
    # Create output
    result = AnalysisResult(
        analysistype=str(analysis_type),
        data=data,
        settings=None,
        metadata=metadata,
        headerdata=headerdata,
        messages=messages,
    )

    return analysis.complete(oncomplete, result)