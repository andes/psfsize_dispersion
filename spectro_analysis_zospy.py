
import zospy as zp

from tqdm import tqdm
from pylab import *
from astropy.modeling import models, fitting
import os
import pathlib
import time
import shutil

import ExtendedImagesSimulation as es

    
class spectro:
    """
    Class to store the spectrograph main caracteristics
    Bldeg: blaze angle in degrees
    opt_d: grating period in lines/mm
    omin: order minimum
    omax: order maximum
    """
    def __init__(self, Bldeg, opt_d, omin, omax):
        self.Bldeg = Bldeg
        self.opt_d = opt_d
        self.omin = omin
        self.omax = omax
    def __str__(self):
        return str(vars(self))
    
def wave_order(spectro, order=88, nwaves=10):
    """
    Gives an verctor sampling an order FSR for a given spectro
    inputs: 
        spectro: class that describes the main characteristics of the spectro
        order: order that needs to be sambpled
        nwaves: number of wavelength to output
    outputs:
        waves: vector of wavelenght covering the order
    
    """
    c_m = 0.5
    lcen = 2*sin(spectro.Bldeg*pi/180)/(order*spectro.opt_d)*1000
    limin = lcen - c_m *lcen/ order
    limax = lcen + c_m * lcen / order
    return linspace(limin,limax,nwaves)


        
        
def geometrical_dispersion_spectro(spectro,oss,nwaves = 10,field = 0, R = 100000):
    """
    Analyze the enlited energy of the PSF of the spectro
    inputs:
        spectro: class that describes the main charateristics of the spectro
        nwaves: number of waves per order where the PSF will be computed
        field: Position of the fiber in the entrance slit in mm
        R = resolution
    outputs:
        data: a table with norders * nwave lines. The column are: 
            order, wavelength, xwave in mm, ywave in mm,
            distance between 2 resolution element in micron along x axis,
            distance between 2 resolution element in micron 
    """

    order_cell = oss.MCE.GetOperandAt(4).GetCellAt(1) 
    wave_cell = oss.MCE.GetOperandAt(1).GetCellAt(1) 
    orderini= order_cell.get_Value()
    waveini = wave_cell.get_Value()

    last_surface = oss.LDE.get_NumberOfSurfaces()
    field1 = oss.SystemData.get_Fields().GetField(1)
    fieldini = field1.get_Y()
    field1.set_Y(field)
    orders = range(spectro.omin,spectro.omax+1)
    data = []
    for o in tqdm(orders):
        waves = wave_order(spectro,o, nwaves)
        for l in range(nwaves):
            order_cell.set_Value(str(o))
            wave_cell.set_Value(str(waves[l]))        
            xb=oss.MFE.GetOperandValue(zp.constants.Editors.MFE.MeritOperandType.CENX,last_surface, 1, 1, 0,5, 0 , 0, 0)
            yb=oss.MFE.GetOperandValue(zp.constants.Editors.MFE.MeritOperandType.CENY, last_surface, 1, 1, 0,5, 0 , 0, 0)
            wave_cell.set_Value(str(waves[l]+waves[l]/R))
            xd=oss.MFE.GetOperandValue(zp.constants.Editors.MFE.MeritOperandType.CENX, last_surface, 1, 1, 0,5, 0 , 0, 0)
            yd=oss.MFE.GetOperandValue(zp.constants.Editors.MFE.MeritOperandType.CENY, last_surface, 1, 1, 0,5, 0 , 0, 0)

            
            ln = (o,waves[l],xb,yb,abs(xd-xb)*1000,sqrt((xd-xb)**2+(yd-yb)**2)*1000)
            data.append(ln)
    
    order_cell.set_Value(str(orderini))
    wave_cell.set_Value(str(waveini)) 
    field1.set_Y(field)
    
    data = array(data)
    figure(1)
    scatter(data[:,2],data[:,3],data[:,4],c=data[:,4]/10)
    colorbar(label = f"pixel for $\Delta\lambda / \lambda$ = {R}")
    figure(2)
    scatter(data[:,2],data[:,3],data[:,5],c=data[:,5]/10)
    colorbar(label = f"pixel for $\Delta\lambda / \lambda$ = {R}")
    return data


def enslited_energy_spectro(spectro,oss, nwaves = 10, field = 0, pixelsize=10, object_size = 0.63):
    """
    Analyze the enlited energy of the PSF of the spectro
    inputs:
        spectro: class that describes the main charateristics of the spectro
        nwaves: number of waves per order where the PSF will be computed
        hx,hy: coordinate of the field at the input in zemax coordinates for computing the position of the wavelength on the detector
        field: Position of the fiber in the entrance slit in mm
        cname: name of the config file that has the right fiber size definition. It will be used to overwrite the current configuration file for the enslited energy analysis
    outputs:
        data: a table with norders * nwave lines. The column are: 
            order, wavelength, xwave in mm, ywave in mm,
            half 80% enslited energy size in x in microns, 
            half 80% enslited energy size in y in microns  
    """
    order_cell = oss.MCE.GetOperandAt(4).GetCellAt(1) 
    wave_cell = oss.MCE.GetOperandAt(1).GetCellAt(1) 
    orderini= order_cell.get_Value()
    waveini = wave_cell.get_Value()

    last_surface = oss.LDE.get_NumberOfSurfaces()
    field1 = oss.SystemData.get_Fields().GetField(1)
    fieldini = field1.get_Y()
    field1.set_Y(field)
    orders = range(spectro.omin,spectro.omax+1)
    data = []
    for o in tqdm(orders):
        waves = wave_order(spectro,o, nwaves)
        for l in range(nwaves):
            order_cell.set_Value(str(o))
            wave_cell.set_Value(str(waves[l]))        
            xb=oss.MFE.GetOperandValue(zp.constants.Editors.MFE.MeritOperandType.CENX,last_surface, 1, 1, 0,5, 0 , 0, 0)
            yb=oss.MFE.GetOperandValue(zp.constants.Editors.MFE.MeritOperandType.CENY, last_surface, 1, 1, 0,5, 0 , 0, 0)
            ee80 = es.ExtendedSourceEncircledEnergy(oss,rays = 5,type ="X Only",  field_size = object_size, maximum_distance = 200)
            EE80 = ee80.Data.to_numpy()
            idx = np.argmin(np.abs(EE80-0.8))
            x80 = ee80.Data.index.to_numpy()[idx]*2
            #print(xb,yb,x80)
            ee80 = es.ExtendedSourceEncircledEnergy(oss,rays = 5,type = "Y Only",  field_size = object_size, maximum_distance = 200)
            EE80 = ee80.Data.to_numpy()
            idy = np.argmin(np.abs(EE80-0.8))
            y80 = ee80.Data.index.to_numpy()[idy]*2

            l = (o,waves[l],xb,yb,x80,y80)
            #print(l)
            data.append(l)

    data = array(data)
    figure()
    scatter(data[:,2],data[:,3],data[:,4],c=data[:,4]/pixelsize)
    colorbar(label = "X 80% energy in pixels")

    figure()
    scatter(data[:,2],data[:,3],data[:,5],c=data[:,5]/pixelsize)
    colorbar(label = "Y 80% energy in pixels")
    return data